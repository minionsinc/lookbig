import static spark.Spark.*;
import spark.*;

public class TwitterStreamServer {
    public static void main(String[] args){

        final TwitterStreamConsumer streamConsumer = new TwitterStreamConsumer(); // final because we will later pull the latest Tweet
        streamConsumer.start();
        //setPort(Integer.parseInt(System.getenv("PORT"))); Uncomment this for Heroku
        get(new Route("/hello") {
            @Override
            public Object handle(Request request, Response response) {
                return "<h1>Tweet Count: " + Integer.toString(streamConsumer.getTweetCount()) + "</h1>" +
                        "<h2>Latest Payload: " + streamConsumer.getLatestTweet() + "</h2>";
            }
        });
    }
}