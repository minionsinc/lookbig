import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;

public class TwitterStreamConsumer extends Thread {

    private static final String STREAM_URI = "https://stream.twitter.com/1.1/statuses/filter.json";
    private String latestTweet;
    private int tweetCount;
   

    public String getLatestTweet(){
        return latestTweet;
    }

    public int getTweetCount(){
        return tweetCount;
    }
    
    private void filter(String json){
    	try{
	    	Class.forName("org.apache.cassandra.cql.jdbc.CassandraDriver");
	        Connection con = DriverManager.getConnection("jdbc:cassandra://localhost:9160/twitter");
	        
	        LinkedList<String> filters = new LinkedList<String>();
	        LinkedList<String> ip = new LinkedList<String>();
	        
	        String checkQuery = "SELECT * FROM filter";
	        PreparedStatement checkStmt = con.prepareStatement(checkQuery);
	        ResultSet result = checkStmt.executeQuery();
	        
	        while(result.next())
	        {
	        	filters.add(result.getString("keyword"));
	        	ip.add(result.getString("ip"));
	        }
	    	if(!filters.isEmpty())
	    	{
		    	int start = json.indexOf("\"text\"");
		        int end;
		        
		        String text = "";
		        String id_str = "";
		        if(start > 0)
		        {
		        	start += 7;
		        	String st = json.substring(start);
		        	end = st.indexOf("\",") + 1;
		        	text = st.substring(0, end);
		        	System.out.println("text: " + text);
		        }
		        Iterator<String> checklist = filters.iterator();
		        int id = 0;
		        while(checklist.hasNext())
		        {
			        if(text.contains(checklist.next()))
			        {
			            start = json.indexOf("\"id_str\"");
			            if(start > 0)
			            {
			            	start += 9;
			            	String st = json.substring(start);
			            	end = st.indexOf("\",") + 1;
			            	id_str = st.substring(0, end);
			            	System.out.println("id_str: " + id_str);
			            }
			            
			            String insertQuery = "INSERT INTO tweet (id_str, text, ip) VALUES (?, ?, ?);";
			            PreparedStatement insertStmt = con.prepareStatement(insertQuery);
			            
			            insertStmt.setString(1, id_str);
			            insertStmt.setString(2, text);
			            insertStmt.setString(3, ip.get(id));
			            
			            insertStmt.execute();
			        } 
			        id++;
		        }
	    	}
    	}catch(Exception e){
	        	System.out.println("LOL EXPLODE");
        }
    }

    public void run()
    {
    	try{
            System.out.println("Starting Twitter public stream consumer thread.");

            // Enter your consumer key and secret below
            OAuthService service = new ServiceBuilder()
                    .provider(TwitterApi.class)
                    .apiKey("QBHk5YmwlSmSQGXuy7VuA") //CONSUMER KEY
                    .apiSecret("MknBXx0VlXX1Bw01Tin7L9VevRTsSJuNr1XOjS3xt4") //CONSUMER SECRET
                    .build();

            // Set your access token
            //ACCESS TOKEN, ACCESS TOKEN SECRET
            Token accessToken = new Token("54750344-mkqOmpqqClhLGUM116UbDMrn4XEeiPcAS2xKYR4j0", "ITo8RD4znl2bNH6XE3Ww5xNLwOi6hC0iQQLnwV5nuYJMC"); 

            // Let's generate the request
            System.out.println("Connecting to Twitter Public Stream");
            OAuthRequest request = new OAuthRequest(Verb.POST, STREAM_URI);
            request.addHeader("version", "HTTP/1.1");
            request.addHeader("host", "stream.twitter.com");
            request.setConnectionKeepAlive(true);
            request.addHeader("user-agent", "Twitter Stream Reader");
            request.addBodyParameter("track", "java,heroku,twitter"); // Set keywords you'd like to track here
          //  request.addBodyParameter("track", "cny");
            service.signRequest(accessToken, request);
            Response response = request.send();
            
           
            
            // Create a reader to read Twitter's stream
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getStream()));

            String line = new String(new byte[10], Charset.forName("UTF-8"));
            FileWriter fw = new FileWriter("test.txt");
            BufferedWriter bw = new BufferedWriter(fw);
            while((line = reader.readLine()) != null)
            {
                filter(line);
			}
        } catch (Exception e){
            //handle the ioe
            e.printStackTrace();
        }

    }
}