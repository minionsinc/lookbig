<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css" media="screen">
		#header {
			/*background:rgba(255,255,255,.5);*/
			background-color: #52b1d4;
			position: absolute;
			top: 0;
			width: 100%;
			height: 25px;   /* Height of the footer */							
		}
		#logo {
			position: fixed;
			top: 10px;  /* adjust value accordingly */
			left: 80px;  /* adjust value accordingly */		
		}
		body {
			font-family: Arial;
			overflow: hidden;
			color: white;
		}
		#slideout {
			position: fixed;
			top: 180px;
			right: 0;
			width: 40px;
			padding: 112px 0;
			text-align: center;
			background: #52b1d4;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			-webkit-border-radius: 20px 0 0 20px;
			-moz-border-radius: 20px 0 0 20px;
			border-radius: 20px 0 0 20px;
			opacity:0.9;
		}
		#slideout_inner {
			position: fixed;
			top: 40px;
			right: -450px;
			background: #52b1d4;
			width: 400px;
			padding: 25px;
			height: 510px;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			-o-transition-duration: 0.3s;
			transition-duration: 0.3s;
			text-align: left;
			-webkit-border-radius: 20px 0 0 20px;
			-moz-border-radius: 20px 0 0 20px;
			border-radius: 20px 0 0 20px;
			font-size:80%;
		}
		#slideout_inner textarea {
			width: 190px;
			height: 100px;
			margin-bottom: 6px;
		}
		#slideout:hover {
			right: 450px;
		}
		#slideout:hover #slideout_inner {
			right: 0;
		}
		#footer {
			text-align: center;
			background-color: #52b1d4;
			color: white;
			position:absolute;
			bottom:0;
			width:100%;
			height:30px;   /* Height of the footer */		
		}
		
		html, body, #map-canvas {
			height: 100%;
			margin: 0px;
			padding: 0px;
		}
		#map-canvas {
			margin: 25px 0px 0px 0px;
		}
</style>

    <script>
function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(-34.397, 150.644)
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
      'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;

    </script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
	  $( "#dialog" ).dialog();
	});
</script>

<script>
var helenus = require('helenus'),
      pool = new helenus.ConnectionPool({
        hosts      : ['localhost:9160'],
        keyspace   : 'helenus_test',
        user       : 'test',
        password   : 'test1233',
        timeout    : 3000
        //cqlVersion : '3.0.0' // specify this if you're using Cassandra 1.1 and want to use CQL 3
      });
      
      pool.on('error', function(err){
    console.error(err.name, err.message);
  });

  //makes a connection to the pool, this will return once there is at least one
  //valid connection, other connections may still be pending
  pool.connect(function(err, keyspace){
    if(err){
      throw(err);
    } else {
      //to use cql, access the pool object once connected
      //the first argument is the CQL string, the second is an `Array` of items
      //to interpolate into the format string, the last is the callback
      //for formatting specific see `http://nodejs.org/docs/latest/api/util.html#util.format`
      //results is an array of row objects

      pool.cql("SELECT col FROM cf_one WHERE key = ?", ['key123'], function(err, results){
        console.log(err, results);
      });

      //NOTE:
      //- You can always skip quotes around placeholders, they are added automatically.
      //- In CQL 3 you cannot use placeholders for ColumnFamily names or Column names.
    }
  });
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>
<body>

<div id="dialog" title="Basic dialog">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

<div id="map-canvas"></div>

<div id="header">
	<p style="margin: 0px 0px 0px 200px; font-weight: bold;">Location:
	<select>
	  <option value="twitter">Singapore</option>
	</select></p>
</div>

<div id="logo">
<img src="logo_v2.png" alt="logo"/>
</div>

<div id="slideout">
  <img src="menu.png" alt="Menu" />
  <div id="slideout_inner">

	<h3>Add New Data Source</h3>
	
	<p>Data Source:
	<select>
	  <option value="twitter">Twitter</option>
	</select></p>
	
	<p>Filter Keywords By:
	<!--<input id="fileSelect" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />-->
	 | <textarea></textarea></p>
	
	<p><input type="submit" value="Submit"></p>

	<hr>
	
	<h3>Existing Data Sources</h3>
	<textarea></textarea>
	
	CSV Export

  </div>
</div>

<div id="footer">
Copyright &copy; 2014 Minions Inc. All Rights Reserved.
</div>

</body>
</html>
